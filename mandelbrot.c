/*
This program is an adaptation of the Mandelbrot program
from the Programming Rosetta Stone, see
http://rosettacode.org/wiki/Mandelbrot_set
Compile the program with:
gcc -o mandelbrot -O4 mandelbrot.c
Usage:

./mandelbrot <xmin> <xmax> <ymin> <ymax> <maxiter> <xres> <out.ppm>
Example:
./mandelbrot 0.27085 0.27100 0.004640 0.004810 1000 1024 pic.ppm
The interior of Mandelbrot set is black, the levels are gray.
If you have very many levels, the picture is likely going to be quite
dark. You can postprocess it to fix the palette. For instance,
with ImageMagick you can do (assuming the picture was saved to pic.ppm):
convert -normalize pic.ppm pic.png
The resulting pic.png is still gray, but the levels will be nicer. You
can also add colors, for instance:
convert -negate -normalize -fill blue -tint 100 pic.ppm pic.png
See http://www.imagemagick.org/Usage/color_mods/ for what ImageMagick
can do. It can do a lot.
*/

/* 
	Name: Kaiden Nunes
	Section: 002

	Compute Times
	1 Thread: 85.504075 seconds
	2 Threads: 54.430616 seconds
	4 Threads: 39.624377 seconds
	8 Threads: 37.197817 seconds
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdint.h>
#include <sys/time.h>
#include <string.h>
#include <omp.h>

/* Return the current time in seconds, using a double precision number. */
double When()
{
	struct timeval tp;
	gettimeofday(&tp, NULL);
	return ((double)tp.tv_sec + (double)tp.tv_usec * 1e-6);
}

int main(int argc, char* argv[])
{
	/* Parse the command line arguments. */
	if (argc != 8)
	{
		printf("Usage:   %s <xmin> <xmax> <ymin> <ymax> <maxiter> <xres> <out.ppm>\n", argv[0]);
		printf("Example: %s 0.27085 0.27100 0.004640 0.004810 1000 1024 pic.ppm\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	double startTime = When();

	/* The window in the plane. */
	const double xmin = atof(argv[1]);
	const double xmax = atof(argv[2]);
	const double ymin = atof(argv[3]);
	const double ymax = atof(argv[4]);

	/* Maximum number of iterations, at most 65535. */
	const uint16_t maxiter = (unsigned short)atoi(argv[5]);

	/* Image size, width is given, height is computed. */
	const int xres = atoi(argv[6]);
	const int yres = (xres*(ymax - ymin)) / (xmax - xmin);

	/* The output file name */
	const char* filename = argv[7];

	/* Open the file and write the header. */
	FILE * fp = fopen(filename, "wb");
	char *comment = "# Mandelbrot set";/* comment should start with # */

									   /*write ASCII header to the file*/
	fprintf(fp,
		"P6\n# Mandelbrot, xmin=%lf, xmax=%lf, ymin=%lf, ymax=%lf, maxiter=%d\n%d\n%d\n%d\n",
		xmin, xmax, ymin, ymax, maxiter, xres, yres, (maxiter < 256 ? 256 : maxiter));

	/* Precompute pixel width and height. */
	double dx = (xmax - xmin) / xres;
	double dy = (ymax - ymin) / yres;

	double y; /* Coordinates of the current point in the complex plane. */
	int i, j; /* Pixel counters */

	/* Array to hold color values */
	unsigned char(*color_array)[xres][6] = malloc(sizeof *color_array * yres * 3);
	
	//omp_set_dynamic(0); 
	//omp_set_num_threads(8);
#pragma omp parallel for private(y, j)
	for (j = 0; j < yres; j++)
	{
		y = ymax - j * dy;
		//omp_set_dynamic(0);
		//omp_set_num_threads(8);
#pragma omp parallel for firstprivate(y, j) private(i)
		for (i = 0; i < xres; i++)
		{
			double u = 0.0;
			double v = 0.0;
			double u2 = u * u;
			double v2 = v*v;
			int k; /* Iteration counter */
			double x = xmin + i * dx;
			/* iterate the point */
			for (k = 1; k < maxiter && (u2 + v2 < 4.0); k++)
			{
				v = 2 * u * v + y;
				u = u2 - v2 + x;
				u2 = u * u;
				v2 = v * v;
			};
			/* compute  pixel color and write it to file */
			if (k >= maxiter)
			{
				/* interior */
				color_array[j][i][0] = 0;
				color_array[j][i][1] = 0;
				color_array[j][i][2] = 0;
				color_array[j][i][3] = 0;
				color_array[j][i][4] = 0;
				color_array[j][i][5] = 0;
			}
			else
			{
				/* exterior */
				color_array[j][i][0] = k >> 8;
				color_array[j][i][1] = k & 255;
				color_array[j][i][2] = k >> 8;
				color_array[j][i][3] = k & 255;
				color_array[j][i][4] = k >> 8;
				color_array[j][i][5] = k & 255;
			};
		}
	}

	double endTime = When();

	printf("\nTime spent: %f seconds\n", endTime - startTime);
	fflush(stdout);

	for (unsigned int row = 0; row < yres; row++)
	{
		for (unsigned int column = 0; column < xres; column++)
		{
			fwrite(color_array[row][column], 6, 1, fp);
		}
	}
	free(color_array);
	fclose(fp);
	return 0;
}